<?php
    date_default_timezone_set("America/La_Paz");
    header('content-type: text/html; charset: utf-8');
    $hora=date("H:i:s");
    $fecha=date("d/m/Y");
    // $ip=$_SERVER['REMOTE_ADDR'];
    $response_captcha = $_POST['g-recaptcha-response'];
    if(isset($response_captcha)&& $response_captcha){
      $secret = "6LckYCQUAAAAABVpDf9uA8em34EnxrcNcXoM8VSK";
      $ip=$_SERVER['REMOTE_ADDR'];
      $validation_server = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response_captcha&remoteip=$ip");
      $array = json_decode($validation_server, true);
      if ($array['success']) {
        // echo "SOY HUMANO";
        $nombre = @trim(stripslashes($_POST['nombre']));
        $movil = @trim(stripslashes($_POST['movil']));
        $empresa = @trim(stripslashes($_POST['empresa']));
        $telefono = @trim(stripslashes($_POST['telefono']));
        $rubro = @trim(stripslashes($_POST['rubro']));
        $servicios = @trim(stripslashes($_POST['servicios']));
        $pais=@trim(stripslashes($_POST['pais']));
        $ciudad = @trim(stripslashes($_POST['ciudad']));
        $direccion = @trim(stripslashes($_POST['direccion']));
        $comentarios = @trim(stripslashes($_POST['comentarios']));

        // $email_to = "hugo@gnb.com.bo";
        $email_to = "nandes.ingsistemas@gmail.com";
        $email_from = @trim(stripslashes($_POST['email']));
        // $email_from = "nandes.ingsistemas@gmail.com";
        $subject = "Registro desde la Web guiasdebolivia.com";

        $body = 'Nombre: '.$nombre."<br>";
        $body .= 'Teléfono móvil: '.$movil."<br>";
        $body .= 'E-mail: '.$email_from."<br>";
        $body .= 'Empresa: '.$empresa."<br>";
        $body .= 'Teléfono: '.$telefono."<br>";
        $body .= 'Rubro: '.$rubro."<br>";
        $body .= 'Productos y/o Servicios: '.$servicios."<br>";
        $body .= 'Pais: '.$pais."<br>";
        $body .= 'Ciudad: '.$ciudad."<br>";
        $body .= 'Direccion: '.$direccion."<br>";
        $body .= 'Comentarios: '.$comentarios."<br>";
        $body .= '---------------------'."<br>";
        $body .= 'Hora y fecha de envío: El '.$fecha.' a las '.$hora."<br>";
        $body .= 'IP del remitente: '.$ip;

        $headers = 'From: '.$email_from."\r\n";
        $headers .= 'MIME-Version: 1.0' ."\r\n";
        $headers .= 'Content-Type: text/HTML; charset=utf-8' ."\r\n";

        if( mail($email_to, $subject, $body, $headers) ){
            echo "enviado";
        }
        else{
            echo "nope";
        }
      }
      else{
        header('Location: http://www.guiasdebolivia.com/');
        exit;
      }
      // var_dump($validation_server);
    }
    else{
      echo "llene";
    }

const gulp = require('gulp'),
      plumber = require('gulp-plumber'),
      /*optimizar imagenes*/
      os = require('os'),
      rename = require('gulp-rename'),
      concurrentTransform = require('concurrent-transform'),
      changed = require('gulp-changed'),
      imagemin = require('gulp-imagemin'),
      pngquant = require('imagemin-pngquant'),
      imageResize = require('gulp-image-resize'),
      cpus = os.cpus().length,

      browserSync = require('browser-sync').create(), //Sincronizar cambios en tiempo real

      // Gulp Plugins
      sass = require('gulp-sass'), //Preprocesador CSS
      postcss = require('gulp-postcss'), //Transpilador CSS
      pug = require('gulp-pug'), //Motor de plantillas para html

     // PostCSS Plugins
      cssnano = require('cssnano'),// Minificacion de css
      pxtorem = require('postcss-pxtorem'),// Convertir valores de px a rem basado en baseFontSize
      //Mapa de archivos generados
      sourcemaps = require('gulp-sourcemaps'),
      //Bundles files
      concat = require('gulp-concat'),
      uglify = require('gulp-uglify');


let postCssPlugins = [
  cssnano({
    autoprefixer: {
      add: true,
      browsers: '> 1%, last 2 versions, Firefox ESR, Opera 12.1'
    },// add autoprefijos css
    // discardComments: false,// se desactivó para manipular comentarios para sass
     core: false // se desactivó para manipular outputStyle para sass
  }),
  pxtorem
];

let resizeAndMinImagesTask = function(options){
  options['imagemin'] = {
    progressive: true,
    interlaced: true,
    svgoPlugins: [{
      removeViewBox: false //https://github.com/svg/svgo/issues/3
    }],
    use: [
      pngquant()
    ]
  }
  return function () {
      return gulp.src("./docs/imgs/**/*.{jpg,jpeg,gif,png,svg}")
      .pipe(rename(function (path) {
        path.basename += "-" + options.width;
      }))
      .pipe(changed("./public_html/images"))
      .pipe(concurrentTransform(imageResize({width: options.width}), cpus))
      .pipe(imagemin(options["imagemin"]))
      .pipe(gulp.dest("./public_html/images"));
    }
};
gulp.task("agropecuaria", resizeAndMinImagesTask({width: 300}));
gulp.task("images", ["agropecuaria"]);
gulp.task('img', function () {
  gulp.watch("./docs/imgs/**/*.{jpg,jpeg,gif,png,svg}", ['images']);
});

gulp.task('sass', ()=>
  gulp.src('./sass/*.scss')
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(plumber())
    .pipe(sass({
      outputStyle: 'compact',
      includePaths: ['node_modules']
    }))
    .pipe(postcss(postCssPlugins))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./public_html/css'))
);
gulp.task('pug', () =>
  gulp.src('./pug/*.pug')
    .pipe(plumber())
    .pipe(pug({
      pretty:true
    }))
    .pipe(gulp.dest('./public_html/'))
);

gulp.task('bundle', () => {
  gulp.src([
    './js/wow.min.js',
    './js/headroom.min.js',
    './js/jquery.js',
    './js/jquery.lazyload.js',
    './node_modules/materialize-css/dist/js/materialize.min.js'
  ])
    .pipe(concat('concat.js'))
    .pipe(rename('bundle.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./public_html/js'))
});


gulp.task('default', () => {
  browserSync.init({
    server: './public_html/'
  });
  gulp.watch('./sass/**/*.scss',['sass']);
  gulp.watch('./pug/**/*.pug',['pug']);
  gulp.watch('./public_html/**/*.html').on('change', browserSync.reload);
  gulp.watch('./public_html/**/*.css').on('change', browserSync.reload);
  gulp.watch('./public_html/**/*.js').on('change', browserSync.reload);
});
